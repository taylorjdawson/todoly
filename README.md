# todoly
View live demo [here](https://todoly.vercel.app)!
## Project setup
```
npm install
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint [--fix]
```

### Run locally

First, you'll need to create an account on [Fauna](https://fauna.com/), then follow these steps:

1. In the [FaunaDB Console](https://dashboard.fauna.com/), click "New Database". Name it whatever you like and click "Save".
2. Click "New Collection", name it `User`, leave "Create collection index" checked, and click "Save".
3. Now go to "Indexes" in the left sidebar, and click "New Index". Select the `User` collection, call it `user_by_email`, and in the "terms" field type `data.email`. Select the "Unique" checkbox and click "Save". This will create an index that allows looking up users by their email, which we will use to log a user in.
4. Next, go to "Security" in the sidebar, then click "New Key". Create a new key with the `Server` role, call it `server-key`, and click "Save". Your key's secret will be displayed, copy that value.
5. Create the `.env` file (which will be ignored by Git) based on the `.env.example` file in this directory:

```bash
cp .env.example .env
```

6. Paste the secret you copied as the value for `FAUNA_SERVER_KEY` in the `.env` file. Keep this key safely as it has privileged access to your database.

> For more information, read the [User Authentication Tutorial in Fauna](https://app.fauna.com/tutorials/authentication).

#### Using vercel
To run locally with vercel use:
```bash
verel dev
```

To deploy to prod use:
```bash
verel --prod
```
