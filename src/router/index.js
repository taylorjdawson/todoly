import Vue from 'vue'
import VueRouter from 'vue-router'
import LoginPage from '../views/LoginPage.vue'
import Login from '../components/Login.vue'
import CreateAccount from '../components/CreateAccount'
import Tasks from '../views/Tasks'
import store from '../store/'

Vue.use(VueRouter)

const guard = (to, from, next) => {
  if (store.getters.isAuthenticated) {
    next()
  } else {
    next('/setup/login')
  }
}

const routes = [
  { path: '/', redirect: { name: 'Tasks' } },
  {
    path: '/tasks',
    name: 'Tasks',
    component: Tasks,
    beforeEnter: guard,
  },
  {
    path: '/setup',
    name: 'Login',
    component: LoginPage,
    children: [
      {
        path: 'login',
        component: Login,
      },
      {
        path: 'signup',
        component: CreateAccount,
      },
    ],
  },
]

const router = new VueRouter({
  mode: 'history',
  routes,
})

export default router
