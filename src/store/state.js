// Fake seed data
const tasks = [
  {
    id: 0,
    listId: 0,
    taskTitle: 'Take out the trash',
    priority: 'critical',
    status: 'todo',
    isComplete: false,
    favorited: false,
  },
  {
    id: 1,
    listId: 0,
    taskTitle: 'Clean the litter box',
    priority: 'low',
    status: 'todo',
    isComplete: false,
    favorited: false,
  },
  {
    id: 2,
    listId: 0,
    taskTitle: 'Build raspberry pi server',
    priority: 'high',
    status: 'in progress',
    isComplete: false,
    favorited: true,
  },
  {
    id: 3,
    listId: 0,
    taskTitle: 'Water the plants',
    priority: 'critical',
    status: 'done',
    isComplete: true,
    favorited: true,
  },
  {
    id: 4,
    listId: 1,
    taskTitle: 'Talking with Strangers',
    priority: 'medium',
    status: 'todo',
    isComplete: false,
    favorited: false,
  },
  {
    id: 5,
    listId: 1,
    taskTitle: 'Animal Farm',
    priority: 'low',
    status: 'todo',
    isComplete: false,
    favorited: false,
  },
  {
    id: 6,
    listId: 1,
    taskTitle: 'Das Capital',
    priority: 'high',
    status: 'done',
    isComplete: true,
    favorited: true,
  },
  {
    id: 7,
    listId: 1,
    taskTitle: 'The Death of Ivan Ilyich',
    priority: 'critical',
    status: 'done',
    isComplete: true,
    favorited: true,
  },
]
const lists = [
  {
    id: 0,
    name: 'chores',
    slug: 'chores',
  },
  {
    id: 1,
    name: 'books to read',
    slug: 'books-to-read',
  },
]

export default {
  isAuthenticated: false,
  user: {
    data: {
      tasks,
      lists,
    },
  },
}
