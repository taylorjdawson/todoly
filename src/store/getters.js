export default {
  isAuthenticated: state => state.isAuthenticated,
  tasks: state => state.user.data.tasks,
  lists: state => state.user.data.lists,
  user: state => state.user,
  sync: state => state.sync
}
