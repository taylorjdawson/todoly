import Vue from 'vue'
import Vuex from 'vuex'

// Global Store
import actions from './actions'
import getters from './getters'
import mutations from './mutations'
import state from './state'
import { post } from "../utils/utils"

Vue.use(Vuex)

const syncData = store => {
  let sync = false
  store.subscribeAction((action) => {
    if(action.type !== 'updateUser' && action.type !== 'updateAuthState') {
      console.log('setting sync flag')
      sync = true
    }
  })
  setInterval(() => {
    if(sync) {
      console.log('syncing data')
      post('/user', { data: store.getters.user.data })
      sync = false
    }
  }, 5000);
}

const store = new Vuex.Store({
  strict: false,
  actions,
  getters,
  mutations,
  state,
  plugins: [syncData],
})

export default store
