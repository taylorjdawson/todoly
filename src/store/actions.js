import { v4 as uuid } from 'uuid'

const Task = ({
  id = uuid(),
  taskTitle = '',
  priority,
  tag,
  isComplete = false,
  isFavorite = false,
  listId,
}) => ({
  id,
  taskTitle,
  priority,
  tag,
  isComplete,
  isFavorite,
  listId,
})

const USER_KEY = 'user'
const AUTH_KEY = 'isAuthenticated'

export default {
  addTask({ state, commit }, { taskTitle, listId }) {
    const { user } = state
    user.data.tasks.push(Task({ taskTitle, listId }))
    commit('UPDATE', { key: USER_KEY, value: user })
  },
  deleteTask({ state, commit }, deletedTask) {
    const { user } = state
    user.data.tasks = user.data.tasks.filter(task => task.id !== deletedTask.id)
    commit('UPDATE', { key: USER_KEY, value: user })
  },
  updateTask({ state, commit }, updatedTask) {
    const { user } = state
    user.data.tasks = user.data.tasks.map(task =>
      task.id === updatedTask.id ? updatedTask : task
    )
    commit('UPDATE', { key: USER_KEY, value: user })
  },
  updateList({ state, commit }, updatedList) {
    const { user } = state
    user.data.lists = user.data.lists.map(list =>
      list.id === updatedList.id ? updatedList : list
    )
    commit('UPDATE', { key: USER_KEY, value: user })
  },
  deleteList({ state, commit }, { deletedListId, retainTasks = true }) {
    const { user } = state
    if (retainTasks) {
      user.data.tasks = user.data.tasks.map(task =>
        task.listId === deletedListId ? Task({ ...task }) : task
      )
    }
    user.data.lists = user.data.lists.filter(list => list.id !== deletedListId)
    commit('UPDATE', { key: USER_KEY, value: user })
  },
  updateAuthState({ commit }, isAuthenticated) {
    console.log({ isAuthenticated })
    commit('UPDATE', { key: AUTH_KEY, value: isAuthenticated })
  },
  updateUser({ commit }, user) {
    if (!Array.isArray(user.data.tasks)) {
      user.data.tasks = []
    }
    if (!Array.isArray(user.data.tasks)) {
      user.data.lists = []
    }
    commit('UPDATE', { key: USER_KEY, value: user })
  }
}
