import ky from 'ky'

const BASE_URL =
  process.env.NODE_ENV === 'production'
    ? 'https://todoly.vercel.app/api'
    : 'http://localhost:3000/api'

export const post = async (route, body) => {
  return ky.post(BASE_URL + route, { json: body })
}

export const get = async route => {
  return ky.get(BASE_URL + route).json()
}
