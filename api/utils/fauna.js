import { Client } from 'faunadb'
import cookie from 'cookie'

export const AUTH_COOKIE_NAME = process.env.AUTH_COOKIE_NAME

export const serverClient = new Client({
  secret: process.env.FAUNA_SERVER_KEY,
})

// Used for any authed requests.
export const faunaClient = (secret) =>
  new Client({
    secret,
  })

export const serializeFaunaCookie = (userSecret) => {
  const cookieSerialized = cookie.serialize(AUTH_COOKIE_NAME, userSecret, {
    sameSite: 'lax',
    secure: process.env.NODE_ENV === 'production',
    maxAge: 72576000,
    httpOnly: true,
    path: '/',
  })
  return cookieSerialized
}
