import { serverClient, serializeFaunaCookie } from './utils/fauna'
import { query as q } from 'faunadb'

const FAUNA_USER_INDEX = 'user_by_email'

export default async  (req, res) => {
  try {
    if(req.method !== 'POST') {
      throw new Error('Endpoint only accepts POST requests')
    }

    const { email, password } = await req.body

    if (!email || !password) {
      throw new Error('Email and password must be provided.')
    }

    const loginRes = await serverClient.query(
      q.Login(q.Match(q.Index(FAUNA_USER_INDEX), email), {
        password,
      })
    )

    if (!loginRes.secret) {
      throw new Error('No secret present in login query response.')
    }

    const cookieSerialized = serializeFaunaCookie(loginRes.secret)

    res.setHeader('Set-Cookie', cookieSerialized)
    res.json({"status": "ok"})
  } catch (error) {
    res.status(400).send(error.message)
  }
}
