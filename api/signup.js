import { query as q } from 'faunadb'
import { serverClient, serializeFaunaCookie } from './utils/fauna'

const USERS_COLLECTION = 'users'
const NOT_UNIQUE = 'instance not unique'
const ERROR_CREATE_USER = 'Error creating new user.'

export default async function signup(req, res) {
  try {
    if(req.method !== 'POST') {
      throw new Error('Endpoint only accepts POST requests')
    }

    const { email, password } = await req.body

    if (!email || !password) {
      throw new Error('Email and password must be provided.')
    }
    console.log(`email: ${email} trying to create user.`)

    let user

    try {
      // Creates a new users and inits the user data with tasks and lists
      user = await serverClient.query(
        q.Create(q.Collection(USERS_COLLECTION), {
          credentials: { password },
          data: { email, tasks: [], lists: [] },
        })
      )
    } catch (error) {
      console.error('Fauna create user error:', error)

      if(error.message === NOT_UNIQUE) {
        throw new Error('User already exists')
      } else {
        throw new Error('Fauna create user error: ' + error)
      }
    }

    if (!user.ref) {
      console.error('Fauna create user error: No ref present in create query response.')
      throw new Error('Fauna create user error: No ref present in create query response.')
    }

    const loginRes = await serverClient.query(
      q.Login(user.ref, {
        password,
      })
    )

    if (!loginRes.secret) {
      console.error('Fauna create user error: No secret present in login query response.')
      throw new Error('Fauna create user error: No secret present in login query response.')
    }

    const cookieSerialized = serializeFaunaCookie(loginRes.secret)

    res.setHeader('Set-Cookie', cookieSerialized)

    res.json({"status": "ok"})
  } catch (error) {
    res.status(400).json({"status": "error", "message": error.message})
  }
}
