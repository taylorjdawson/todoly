import cookie from 'cookie'
import { query as q } from 'faunadb'

import { faunaClient, AUTH_COOKIE_NAME } from './utils/fauna'
import { isPost } from "./utils/helpers"

export const userId = async (faunaSecret) => faunaClient(faunaSecret).query(q.Identity())

const getUserData = async authToken => await faunaClient(authToken).query(
  q.Get(
    q.Identity()
  )
)

const updateUserData = async (authToken, data) => await faunaClient(authToken).query(
  q.Update(
    q.Ref(q.Collection('users'), q.Select( "id",
      q.Identity()
    )),
    { data },
  )
)

export default async function user(req, res) {
  const cookies = cookie.parse(req.headers.cookie || '')
  const authToken = cookies[AUTH_COOKIE_NAME]

  const { email } = req.query

  if(isPost(req)) {
    const { data } = req.body
    console.log(data)
    await updateUserData(authToken, data)
  }

  if (!authToken) {
    return res.status(401).send('Auth cookie missing.')
  }

  const { data } = await getUserData(authToken, email)

  res.status(200).json({ data })
}
