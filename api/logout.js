import { query as q } from 'faunadb'
import cookie from 'cookie'
import { faunaClient, AUTH_COOKIE_NAME } from './utils/fauna'

export default async function logout(req, res) {
  const cookies = cookie.parse(req.headers.cookie ?? '')
  const faunaSecret = cookies[AUTH_COOKIE_NAME]

  if (!faunaSecret) {
    // Already logged out.
    return res.status(200).end()
  }

  // Invalidate secret (ie. logout from Fauna).
  await faunaClient(faunaSecret).query(q.Logout(false))

  // Clear cookie.
  const cookieSerialized = cookie.serialize(AUTH_COOKIE_NAME, '', {
    sameSite: 'lax',
    secure: process.env.NODE_ENV === 'production',
    maxAge: -1,
    httpOnly: true,
    path: '/',
  })

  res.setHeader('Set-Cookie', cookieSerialized)
  res.json({"status": "ok", "message": "user logged out"})
}
